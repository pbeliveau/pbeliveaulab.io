---
layout: post
title: Exporting an Excel file to CSV
description: Why plain text files are better and how can you use VBA to export to .CSV
cover_url: /img/xlsx2csv/charles-deluvio-456501-unsplash.png
---

# Table of Contents

1.  [Why you should use CSV?](#orga205a3b)
2.  [When to export in CSV?](#org0a57cb5)
3.  [How to export to CSV](#orgbea750a)
    1.  [Built-in Excel convert](#orgb304d7e)
    2.  [Concatenate and copy-paste](#orgaefd8fd)
    3.  [Use VBA to generate the CSV](#orgc24f278)



<a id="orga205a3b"></a>

# Why you should use .CSV?

Let's get something out of the way, CSV means ["comma-seperated
values"](https://en.wikipedia.org/wiki/Comma-separated_values). It's a plain
text file with a .CSV extension.

I've you ever wondered why most banks allows you to export your transaction data
to .CSV files and not some variant of the .xlsx of Excel? It's quite simple
actually: .CSV is forever. [Or more accuratly, plain text is
forever.](https://en.wikipedia.org/wiki/Plain_text#Usage) It's nothing more than
ANSI characters. If I went back 50 years in the past, or the future for that
matter, I could easily open a .CSV file and use what's inside because it's plain
text. Trying doing the same with the non sense that are proprietary formats like
xlsx/xls/xlsm/etc. is nightmarish.

If you feel like wasting time, here is how to see how bad it is:

1.  Go to a directory with a .xlsx file;
2.  Change the extension to .zip (Make a copy of it first);
3.  Open the .zip file to see what compose an Excel spreadsheet;
    ![img](/img/xlsx2csv/xlsx_structure.png)
4.  With your favorite text editor, open one of the file located in
    xl/worksheets/\*.xml ![img](/img/xlsx2csv/xmlsheet.png)
5.  If you did not like CSV before, I bet you do now. ![img](/img/xlsx2csv/xmlblob.png)

Moreover, CSV is light. It can hold millions of entries without breaking a
sweat. On the other hand, Excel, as of right now, can't handle files larger than
2GB (circa 2018).

<a id="org0a57cb5"></a>

# When to export in CSV?

At some point, if you have an Excel file that holds a database and want
to do one of the following :

-   Use a SAS language like R or Python to analysis the data;
-   Import the data into an Enterprise Resource Planing software that does not
    support Excel formats;
-   Becomes too big to be handled with Excel;
-   Backup in a format with more support for debugging in the future.

If anything of the above is applicable to your situation, you're on the right
website, keep reading.

<a id="orgbea750a"></a>

# Ways to export to CSV (3 known methods)

<a id="orgb304d7e"></a>

## Built-in Excel convert

This is the easiest method. If you have no trust whatsoever in your coworkers,
that's the method you recommend and show them. Here are the three simple steps:

1.  Select the sheet you want to export as .CSV;
2.  In the menu, select "File", "Export", "Change File Type", and select "CSV
    (Comma delimited)";
3.  Dismiss the message saying you can only export one sheet at a time in CSV.

The simplicity of this method removes all control to change how to file is
exported. Plus, you have to repeat the same steps several times if have multiple
sheets to export. That can amount to productive minutes/hours wasted.

Even if you only have one sheet to export, the built-in way can be somewhat slow
and/or buggy. If your sheet is complex the resulting CSV file could be filled
with error difficult to catch afterward. 

Although, it's the easiest method to use if your technical skills are poor
and/or your coworkers are amateurish when it comes to using computers. 

<a id="orgaefd8fd"></a>

## Concatenate and copy-paste

Yes, you actually can do that. I didn't realize that was a possibility until I
saw coworkers do it to generate accounting entries to be imported in an ERP.

This method as several shortcomings. First, you are entirely dependent on the
amount of physical RAM your system has. Windows clipboard relies on the RAM to
store the data you copy. Considering Windows 10 uses a good 2GB of RAM
just to start plus the size of your Excel file, it leaves you less than 2GB if
your computer comes with the very low amount of 4GB RAM (sad). 

Second, this method is error prone for multiple reasons. It requires the end
users to do too many steps by themselves :

1.  Create string in a single cell with every column value separated by the
    right delimiter; (Risk of creating an inadequate formula)
2.  Drag-down the formula for all the rows of the database; (Risk of missing
    some data or using the wrong data)
3.  Create UTF-8 text file with the right extension of the file (if
    needed); (Risk of user not creating the right type of file)
4.  Copy generated values in excel and paste it.

However, this method is very useful if you only want to transfer a very small
amount of data to a text file. Let's say a coworker wants the weather data for
yesterday per hour. You can filter with the date your sheet. It's faster and
less risky considering the small amount of data. Plus, you can reuse the formula
created in step 1 (see above).

<a id="orgc24f278"></a>

## Use VBA to generate the CSV

Now, if you agree with my assessments of the two preceding methods, this is for
you. I wrote a macro that you can use right away to export to CSV. The
repository holding the code is here :
<https://gitlab.com/pbeliveau/excel-to-csv>

Here is a demonstration of how to install and use :
![](https://gitlab.com/pbeliveau/excel-to-csv/raw/master/img/example.gif)

This method as several advantages :

-   You can modify the code to adapt it to your needs; (try doing that with
    the built-in function)
-   You can create a spreadsheet with the macro and ask your end users to use
    it instead of the two methods above. This eliminates provides a seamless
    experience for your users and reduces the risk of errors from the other
    methods;
-   It's fast. Blazing fast. Because every row becomes a line and is written one at
    a time to the new CSV file, nothing is stored on the computer RAM; (the real
    bottleneck becomes the disk I/O speed)

With a little knowledge of VBA, you can add or remove features to this algorithm
easily. As an example, with some tweaks, you can generate one big CSV for
multiple sheets of the same Excel file at once.

Unless you are fluent in other programming languages, don't use something else
 than VBA. Why? VBA is compiled directly into Excel and executes much faster
 than any other comparable programming languages.  (See this answer from a user
 that determined it was 6 times faster than Python: [Demonstration of Python
 vs. VBA](https://stackoverflow.com/questions/30045508/python-loop-slower-than-excel-vba)).

Albeit, if you know some Python and performance is not mission critical, just
use Python. VBA is on the way out. [Excel team at Microsoft is considering
adding support for it
natively](https://excel.uservoice.com/forums/304921-excel-for-windows-desktop-application/suggestions/10549005-python-as-an-excel-scripting-language)
and [VBA is one of the most hated language out
there.](https://boxbase.org/entries/2014/jul/28/vba/)
