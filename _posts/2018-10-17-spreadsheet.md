---
layout: post
title: Spreadsheet efficacy and when it fails
description: The importance of Microsoft Excel and good practice
cover_url: /img/annie-spratt-439326-unsplash-optimized.png
---

# Table of Contents


1.  [Why Excel and spreadsheets are amazing](#org459c633)
2.  [When it falls short](#orgfb05c3f)
    1.  [Indicators of a suboptimal workbook](#org6d4594d)


<a id="org459c633"></a>

# Why Excel and spreadsheets are amazing

A couple of days ago [I read an article from
HBR.org](https://hbr.org/2018/10/10-excel-functions-everyone-should-know) about
spreadsheets. The intro mentioned the place Microsoft Excel has in our
lives. Specifically, it said that at any given time, 750 millions people use
Excel in the world and that the average worker will spend 10% of their work life
using it. I've worked at three major companies during my accounting studies. As
you can guess, I was employed in the finance and/or reporting departments.

Now, all of those organizations have pricey ERPs ([real
pricey&#x2026;](https://www.cio.com/article/2373469/enterprise-software/want-to-save--10-million-or-more-on-erp--don-t-buy-oracle-or-sap.html)). Still, >99% of my time was spent working on Excel files to accomplish various tasks. I
won't get into the subject of why employees and their supervisors don't use ERPs
even when the license is already paid. Subject for another post.

However, I can tell you why they use spreadsheet software :

-   It's known. In fact, everyone know how to operate them. If you send a .xlsx
    file to someone, they know how to open it, how to manipulate it, etc. This may
    sound obvious, but believe me, it's hard to find something that you are
    certain every single employee and business understand <span
    class="underline">and</span> uses.
-   It's flexible. It can do anything you throw at it. It can crunch numbers,
    process documents, prepare presentations, act as a business intelligence
    tool (BI). You get the gist. Not a lot of tools are as flexible as a
    spreadsheet software.
-   It's easy. It as a simple graphic interface that everyone
    comprehend seamlessly. It's intuitive and it feels like something
    we've been using since the first time we ever came in contact with
    computers. (Excel has been a Microsoft product for more than 30
    years).
-   It's cheap. Not dirt cheap, but close. Last time I verified, it was 11$ per
    month per user for the whole Office suite of software. So even your
    *mom-and-pop* small shop can afford it. If they can't, there is other viable
    options, like [LibreOffice](https://www.libreoffice.org/).
-   It's useful. Want to find out how much it costs per unit of *x*? Not a
    problem. Excel is perfect for those ad hoc reports that your boss wants
    today.


<a id="orgfb05c3f"></a>

# When it falls short

Sounds too good to be true? Here comes the catch: **Everyone** uses it. By
everyone, I mean it. Old, young, no experience, a lot of it, etc. Do you see the
problem now? No standard of working, millions of way to create the layout, and
to manage those spreadsheets. This result in the utmost variety of quality of
workbooks.

When spreadsheet workbooks are badly constructed, the outcome can be as benign
as poor efficiency to outright [financial
disasters](https://www.forbes.com/sites/timworstall/2013/02/13/microsofts-excel-might-be-the-most-dangerous-software-on-the-planet/#5503c85e633d). Remember
the [*London Whale* scandal of
2012?](https://en.wikipedia.org/wiki/2012_JPMorgan_Chase_trading_loss) The
losses of JP Morgan were multiplied because they were copy-pasting between
workbooks and the layouts were not straight forward, resulting in mistakes.

Some organizations recognized these dangers and try to correct them.
A good chunk of my time as a financial analyst was to open workbooks used in
critical parts of a company, take note of the damages, plan for repairs, or if
the document is too flawed, make a brand-new one.


<a id="org6d4594d"></a>

## Indicators of a suboptimal workbook

When I open a spreadsheet I've never worked with, I look for a handful of
elements to determine whether it's salvageable or not. Here are the indicators
to find if you are dealing with a **damaged** workbook:

-   Workbook created a decade ago. Business needs change over time, it
    grows, it diversifies, etc. As a result, the spreadsheets used to
    follow and drive those processes need to be updated. In the
    software world, they call this a revision.
-   An increasing number of rotating employees had to use it over the years.
    When an increasing number of people worked on the same workbook without
    guidelines and/or without talking to each other, you run the risk of
    creating a monster: Every sheet looks completely different, you can't trace
    back the changes, and the number of links between sheets defy logic.
-   Level of importance to the business. I am a believer in making internal
    process as efficient as possible. When the operation of a spreadsheet is
    critical to the organization, the tolerance of inefficiency becomes way
    lower.
-   No documentation to support users of the Excel application. As an example,
    it requires another employee to take time out of his schedule to *teach* how
    to use it. Very little companies have documentation regarding the usage of
    their workbooks. As far as I'm concerned, documentation is as important as
    the workbook itself to debug, understand, and use efficiently the
    spreadsheet.
-   No one can explain in a single concise phrase how the workbook works and
    what it does. (No guarantee of result) A spreadsheet needs to have a
    specific goal, like determining the interest rates for 5 years bonds. If it
    does too many things, it has many breakage points instead of a single one.
-   A messy layout. This one is more complicated to explain but might be the
    most important indicator that a workbook is too far gone to be
    saved. Usually, you know 30 seconds into opening a spreadsheet and moving
    around if the layout works and makes sense. If you're scratching your head
    and asking yourself 'what the heck am I looking at?'&#x2026; It's a messy
    layout.

>Take note that the indicators are not to be use for an
>ad hoc spreadsheet. (i.e. report that will be use once.)

If you score 3 out of the 6 indicators, it might be time to retire this
workbook. It's faster to start from zero with a fresh and solid base than doing
the impossible.  Making a good spreadsheet for years to come is not a big
ordeal, and it does not require you to be an Excel MVP ([yes, it's a real
thing](https://www.mvp.microsoft.com/en-us/MvpSearch?lo=Canada&kw=excel&sc=e)).
Just do the inverse of the indicators of a bad spreadsheet and you should be
more than fine.

## Resources

If you can, schedule time to read and learn on how to get better with
Excel. To get you started, here are a few good reads to build robust
workbooks in Excel :
- [Excel Spreadsheets Best Practices by Andrew
  Chan](https://www.soa.org/News-and-Publications/Newsletters/Compact/2015/march/Excel-Spreadsheets-Best-Practices.aspx)
  , a good walkthrough in making durable and efficient Excel applications.
- [Twenty principles for good spreadsheet practice by the Institute of Chartered
  Accountants in England and
  Wales](https://www.icaew.com/technical/technology/excel/twenty-principles),
  solid framework for manager regarding the use of spreadsheet.
- [Tidy data by Hadley Wickham](http://vita.had.co.nz/papers/tidy-data.html), de
  facto article on how to present data (comes back to the layout issue) and,
  thus, save time.
