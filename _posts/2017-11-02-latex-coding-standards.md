---
layout: post
title: 
description:
cover_url: /img/latex-coding-standards.jpg
---

_<center>Accurate depiction of the current state of coding style for LaTeX</center>_

## Brief history
Creating coding conventions
like [PEP-8 for Python](https://www.python.org/dev/peps/pep-0008/) or the
famous [Linux coding style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html) 
for the kernel
development in C was _never_ a priority with LaTeX. Why? Well, all the
reasons to create a coding standard never were really present for LaTeX
and the use of the language. Usually, a coding standard emerges when:

1. Multiple people collaborate on a project. Cohesion and quality of the
code can then become an issue without a standard.
2. The code is audited or might be and thus code quality and readability
become an important factor.
3. A project might be used and refactored into other projects and 
compatibility becomes an important factor.
4. The project must be supported for multiple years and a convention
becomes important so that other people than the first creator can work
without having to firstly understand the specific way a project was
coded.

But for those familiar with LaTeX, rarely does a project in 
LaTeX is complex enough to create a standard of coding practices. Indeed,
most TeX documents are initially created for personal use like a C.V. or
school work, etc.
Plus, only a few projects are maintained across multiple years and
most of the time those projects are behind closed doors for private
entities like a corporation generating monthly reports.

So for the past 32 years ([since the creation](https://en.wikipedia.org/wiki/LaTeX) 
of LaTeX), establishing a coding standard was not a pressing matter.
Consequently, there is no coding style agreed upon by a large portion
of LaTeX user.

## No standard, is that a bad thing?

Yes.

If you ever had to work on a large project, like writing a book with more
than yourself (multiples authors), you know what kind of hell it can be
to maintain a certain level of readability when everyone writes their code
differently. How many times do I have to review a text written like the
author forgot to break lines :

```latex
\begin{document}
this is a very long line that I wrote to show that editing a mistake made in the middle of this line will be more complex to address than someone breaking lines after a number of characters. some people use \latex like a WYSIWYG and they tend to forget they can make the text much more readable by ppressing "enter" on their keyboards from time to time.
```

This is only a small glimpse into the readability problems resulting from 
using different styles while working simultaneously on a document. Other
obvious issues that occurs without style are the capacity to maintain and
build on an existing project promptly.

## Enters Didier Verna

At the _Tex Users Group_ annual conference of 2011, Didier Vernais 
presented his vision of a coding standard for LaTeX. You can find 
the following paper freely available here: 
[Towards LaTeX coding standards](https://www.tug.org/TUGboat/tb32-3/tb102verna.pdf)

Like myself, Didier had his fair share of scrapes. He goes as far as to
qualify the present situation in the world of LaTeX as being close to
anarchy. Browsing the GitHub repos containing tex files, 
I would say that it's a fair assertion. (not to say that languages having
code styles, like Python, don't have to deal with the same problems)

I think _Towards LaTeX coding standards_ brings a lot of simple and logical
ideas that would increase the quality of LaTeX of documents.
This is a summary of the proposed coding standard made by Didier. I
would advise reading the whole document when you have time. Otherwise, 
this is to highlight elements of the paper so that this post can
be serve as some sort of cheatsheet in writing robust and consistent LaTeX
documents for the masses.

Notice: this only cover elements of the Didier paper on layout of
documents (low level style) and not styles standards regarding higher
abstraction levels like packages, conflict management, etc. Please refer
to the whole document for those components of Didier style.

## The Didier Coding Style (DCS)

Intended user:
- You are writing documents in LaTeX in collaboration with other people; or/and
- You want to make your LaTeX documents readable and robust; or/and
- You are a perfectionist, like me.

I only address a small part of DCS because he treats mostly about coding
style for LaTeX packages developers while I only want to cover the generic rules that
should apply in regular documents.

### Rule 1: Stay WYSIWG coherent

Essentially: Do not only think about what the resulting document will
look like. You have to think about what the format of your code like it
is itself the output. Some basic examples:

- A blank line after a paragraph means the end of the paragraph:
   ```latex
  This is my first paragraph.
  
  This is a second paragraph.
  ```
  
- When using '\\\\' or '\par', you should not write text following that statement. (meaning on the same line)
   ```latex
  This is my first paragraph. \\
  This is a second paragraph.
  ```
  
- Tabular environment should mimic the actual output layout even if it
increases the amount of time to realize. Meaning, should you be able to
read a table without producing the output.

Yes:
   ```latex
    \begin{tabular}{ |p{3cm}||p{3cm}|p{3cm}|p{3cm}|  }
	 \hline
	 \multicolumn{4}{|c|}{Country List} \\
	 \hline
	 Country Name   & ISO ALPHA 2 Code & ISO ALPHA 3 Code & ISO numeric Code\\
	 \hline
	 American Samoa & AS  		   & ASM              & 016 \\
	 Andorra        & AD               & AND              & 020     \\
	 Angola         & AO               & AGO              & 024     \\
	 \hline
    \end{tabular}
  ```
  
No:
   ```latex
    \begin{tabular}{ |p{3cm}||p{3cm}|p{3cm}|p{3cm}|  }
     \hline
     \multicolumn{4}{|c|}{Country List} \\
     \hline
     Country Name&ISO ALPHA 2 Code&ISO ALPHA 3 Code&ISO numeric Code\\
     \hline
     American Samoa&   AS  & ASM&016\\
     Andorra& AD  & AND   &020\\
     Angola& AO  & AGO&024\\
     \hline
    \end{tabular}
  ```

### Rule 2: Spaces in math mode

This one is pretty straight forward: give some room to your text in math
mode like you would do in regular text.

Yes:
```latex
$ f(x) = 2 + f(x-2) $
```

No:
```latex
$f(x)=2+f(x-2)$
```

### Rule 3: One instruction per line

This is one instruction as one logical instruction per line. 
Not to be confused with simply a single instruction (logical vs. unique).
Here is an example:

Yes:
```latex
\hskip.11em\@plus.33em\@minus.07em
\small
```

No:
```latex
\hskip.11em\@plus.33em\@minus.07em \small
```
### Rule 4: Indent all forms of grouping 

The most simple and yet the most important rule. Please indent when it's
logical to do so. Didier has a great example of indentation :

Yes:
```latex
{% raw %}\def\@docinclude#1{% {% endraw %}
    \clearpage
    \if@filesw\immediate\write\@mainaux{\string\@input{#1.aux}}\fi
    \@tempswatrue
    \if@partsw
	\@tempswafalse
	\edef\@tempb{#1}
	\@for\@tempa:=\@partlist\do{\ifx\@tempa\@tempb\@tempswatrue\fi}%
    \fi
    \if@tempswa
	\let\@auxout\@partaux
	\if@filesw
	    \immediate\openout\@partaux #1.aux
	    \immediate\write\@partaux{\relax}%
	\fi
    \fi
```

Please  no...
```latex
\def\@docinclude#1{\clearpage
\if@filesw\immediate\write\@mainaux{\string\@input{#1.aux}}\fi
\@tempswatrue\if@partsw\@tempswafalse\edef\@tempb{#1}\@for
\@tempa:=\@partlist\do{\ifx\@tempa\@tempb\@tempswatrue\fi}\fi
\if@tempswa \let\@auxout\@partaux \if@filesw
\immediate\openout\@partaux #1.aux
\immediate\write\@partaux{\relax}\fi
% ... \fi :-(
```

### Rule 5: Be modular
This rule is not treated in the formatting section in Didier paper. In
fact, Didier mostly address the modularity aspect from a package developer standpoint. However, I would like to add this rule to the
formatting section for one obvious reason: Working with 10 small files is easier and than working with one big file. Don't agree? Let's go back to
rule 4 about indentation and let's say we are writing a book. It's
easier to get a grasp of the code if this is the structure :

Yes:
```
+-- book
|   +-- book.tex
|   +-- chapters
|       +-- chapter1.tex
|       +-- chapter2.tex
|       +-- ...
```

book.tex file
```latex
\begin{document}
\input{chapters/chapter1.tex}
\input{chapters/chapter2.tex}
...
\end{document}
```

No:

```
+-- book
|   +-- book.tex
```

book.tex file
```latex
\begin{document}
% 3000 lines later ...
\end{document}
```
