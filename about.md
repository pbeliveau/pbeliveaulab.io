---
layout: page
title: About
lang: en
permalink: /about/
navlevel: header
navtitle: about
cover_url: /img/RideauDawn.JPG
---

I am completing an MBA specialized in accounting at
[UQTR](https://www.uqtr.ca/). I passed the Common Final Exam to become CPA under
the CPA Canada order.

I live in beautiful Ottawa ([see picture I took](/img/RideauDawn.JPG)). The best place to live in Canada
according to 
[MoneySense](https://www.moneysense.ca/canadas-best-places-to-live-create-your-own-ranking/).

I'm passionate about open-source software, data (the more the merrier), and
learning new stuff. During my years of working as a financial analyst, I've
worked extensively with Microsoft Excel and VBA.
